﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FashionDesign.Controllers
{
    public class FashionController : Controller
    {
        FashionDesignEntities Fashion = new FashionDesignEntities();
        // GET: Fashion
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ArticleDetails()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ArticleDetails(FormCollection Article)
        {
            fashion_article_details Art = new fashion_article_details();
            string Image = null;
            if (!string.IsNullOrEmpty(Article["Hidden_AttributeImage"]))
            {
                if (!string.IsNullOrEmpty(Request.Files["AttributeImage_Name"].FileName))
                {
                    HttpPostedFileBase SearchImage = Request.Files["AttributeImage_Name"];
                    Image = fu.CommonCategoryImageUpload(SearchImage);
                }
                else
                    Image = Article["Hidden_AttributeImage"];
            }
            return View();
        }

    }
}