﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FashionDesign.Startup))]
namespace FashionDesign
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
